//
//  ToDoItemView.swift
//  ToDo
//
//  Created by Tareq Saifullah on 19/03/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import SwiftUI

struct ToDoItemView: View {
    var title:String = ""
    var createAt:String = ""
    @State var isDone:Bool = false
    
    var body: some View {
        HStack{
            Button(action:{
                self.isDone.toggle()
            }){
                if isDone{
                    Image(systemName: "circle.fill")
                }else{
                    Image(systemName: "circle")
                }
            }
            VStack(alignment: .leading){
                Text(title)
                    .font(.headline)
                Text(createAt)
                    .font(.caption)
            }
        }
    }
}

struct ToDoItemView_Previews: PreviewProvider {
    static var previews: some View {
        ToDoItemView()
    }
}
