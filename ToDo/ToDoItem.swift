//
//  ToDoItem.swift
//  ToDo
//
//  Created by Tareq Saifullah on 19/03/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import Foundation
import CoreData

public class ToDoItem:NSManagedObject,Identifiable{
    @NSManaged public var createdAt:Date?
    @NSManaged public var title:String?
    @NSManaged public var done:Bool
    
}
extension ToDoItem{
    static func getAllToDoItem()->NSFetchRequest<ToDoItem>{
        let request:NSFetchRequest<ToDoItem> = ToDoItem.fetchRequest() as! NSFetchRequest<ToDoItem>
        
        let sortDescriptor = NSSortDescriptor(key: "createdAt", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        return request
    }
}
